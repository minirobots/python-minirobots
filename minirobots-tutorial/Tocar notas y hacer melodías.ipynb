{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Tocar notas y hacer melodías\n",
    "\n",
    "### *Aprenderemos a encender y apagar los leds de la tortuga*\n",
    "\n",
    "El robot tortuga puede hacer lo siguiente:\n",
    "\n",
    "- Moverse hacia adelante o hacia atrás\n",
    "- Girar a la derecha o a la izquierda\n",
    "- Subir o bajar el lápiz\n",
    "- Encender o apagar los leds\n",
    "- **Reproducir sonidos**\n",
    "\n",
    "En este capítulo aprenderemos cómo hacer sonidos utilizando las siguientes funciones:\n",
    "\n",
    "- `play_random_note`\n",
    "- `play_note`\n",
    "- `silence`\n",
    "\n",
    "Cualquiera de estas acciones las podemos realizar mediante el objeto tortuga. Creemos uno:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from minirobots import Turtle\n",
    "\n",
    "turtle = Turtle(\"15354b\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "La tortuga puede tocar notas reales, es decir Do, Re, Mi, Fa, Sol, La y Si en varias octavas. Para probar cómo suenan podemos empezar probando la función `play_random_note`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "turtle.play_random_note()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Ejecutemos varias veces la función `play_random_note` para **oir** y **ver** las distintas notas que puede tocar la tortuga, ya que esta función nos retorna la nota ejecutada.\n",
    "\n",
    "La función `play_random_note` también recibe un parámetro opcional con la duración en milisegundo de la nota a tocar. Por defecto es `1000ms` (un segundo), así que probemos ejecutar varias notas al azar haciéndolas durar un segundo cada una:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from time import sleep\n",
    "\n",
    "for _ in range(20):\n",
    "    note = turtle.play_random_note(1000)\n",
    "    print(note, end=\" \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Aquí podemos ver la lista completa de notas que puede ejecutar la tortuga. Están en orden ascendente, es decir, de la nota más grave a la más aguda:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for note in Turtle.NOTES.keys():\n",
    "    print(note, end=\", \")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Pero, ¿qué significan esos símbolos?\n",
    "\n",
    "Las notas se representan usando lo que se conoce como [Cifrado Americano](https://es.wikipedia.org/wiki/Sistema_de_notaci%C3%B3n_musical_anglosaj%C3%B3n). En el cifrado americano las notas musicales se cambian por las letras del alfabeto que van de la A a la G.\n",
    "\n",
    "![](notas_musicales.png)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Así que si queremos ejecutar un `La` escribimos `A`, si queremos un `Sol` escribimos `G` y así.\n",
    "\n",
    "Además la octava se representa con los números `1 - 4` (En la octava 5 sólo hay un Do)\n",
    "\n",
    "Por lo que\n",
    "- `C1` es un `Do` en la octava `1`\n",
    "- `B4` es un `Si` en la octava `4`\n",
    "\n",
    "También tenemos las alteraciones de las notas, podemos tener sostenidos `#` y bemoles `b`. Este símbolo en notación musical va inmediatamente seguido de la nota :)\n",
    "\n",
    "Aquí van varios ejemplos finales:\n",
    "\n",
    "| Nota  | Significado            |\n",
    "|:------|:-----------------------|\n",
    "| `C#1` | Do sostenido, octava 1 |\n",
    "| `Db3` | Re bemol, octava 3     |\n",
    "| `F2`  | Fa, octava 2           |\n",
    "| `A#2` | La sostenido, octava 2 |\n",
    "| `Gb4` | Sol bemol, octava 4    |\n",
    "| `B2`  | Si, octava 2           |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Hagamos alguna melodía\n",
    "\n",
    "Ahora que sabemos las notas que puede tocar la tortuga podemos usar la función `nota` para armar una secuencia de notas para tocar alguna melodía. La función `nota` recibe dos parámetros, la nota a tocar y la duración de la misma.\n",
    "\n",
    "Pero empecemos por hacer sonar en orden la primera octava de la escala natural de Do: `C1`, `D1`, `E1`, `F1`, `G1`, `A1`, `B1`, `C2`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "duration = 1000 # en milisegundos\n",
    "turtle.play_note('C1', duration)\n",
    "turtle.play_note('D1', duration)\n",
    "turtle.play_note('E1', duration)\n",
    "turtle.play_note('F1', duration)\n",
    "turtle.play_note('G1', duration)\n",
    "turtle.play_note('A1', duration)\n",
    "turtle.play_note('B1', duration)\n",
    "turtle.play_note('C2', duration)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Excelente!\n",
    "\n",
    "Ahora veremos una técnica para tocar una melodía cualquiera. La técnica se basa en definir un valor para las negras y luego definir corcheas, blancas y cualquier otra duración en base a dicho valor. De esta forma podemos referirnos a la duración de las notas en referencia al valor de la negra y cambiar el tiempo de la melodía simplemente modificando una única variable.\n",
    "\n",
    "Veamos como sería, por ejemplo, El feliz cumpleaños:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "quarter = 500        # negra\n",
    "eighth = quarter / 2 # corchea\n",
    "half = quarter * 2   # blanca\n",
    "\n",
    "turtle.play_note('G1', eighth)\n",
    "turtle.play_note('G1', eighth)\n",
    "turtle.play_note('A1', quarter)\n",
    "turtle.play_note('G1', quarter)\n",
    "turtle.play_note('C2', quarter)\n",
    "turtle.play_note('B1', half)\n",
    "\n",
    "turtle.play_note('G1', eighth)\n",
    "turtle.play_note('G1', eighth)\n",
    "turtle.play_note('A1', quarter)\n",
    "turtle.play_note('G1', quarter)\n",
    "turtle.play_note('D2', quarter)\n",
    "turtle.play_note('C2', half)\n",
    "\n",
    "turtle.play_note('G1', eighth)\n",
    "turtle.play_note('G1', eighth)\n",
    "turtle.play_note('G2', quarter)\n",
    "turtle.play_note('E2', quarter)\n",
    "turtle.play_note('C2', quarter)\n",
    "turtle.play_note('B1', quarter)\n",
    "turtle.play_note('A1', quarter)\n",
    "\n",
    "turtle.play_note('F2', eighth)\n",
    "turtle.play_note('F2', eighth)\n",
    "turtle.play_note('E2', quarter)\n",
    "turtle.play_note('C2', quarter)\n",
    "turtle.play_note('D2', quarter)\n",
    "turtle.play_note('C2', half)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## A practicar!\n",
    "\n",
    "¿Te animás a crear alguna melodía?"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Avancemos!\n",
    "\n",
    "Próximo capítulo: [Múltiples tortugas](Múltiples%20tortugas.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3.7.4 64-bit ('env': venv)",
   "language": "python",
   "name": "python37464bitenvvenv87dbb8ee0cea43de8d92d60a4e873277"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.7.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
