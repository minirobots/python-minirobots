# Distribute package

## Build

```sh
$ bin/build.sh
```

## Clean

```sh
$ bin/clean.sh
```

## Test

```sh
$ python -m pip install dist/minirobots_turtle-0.2.9-py3-none-any.whl
```

## Upload

```sh
$ bin/upload.sh
```
