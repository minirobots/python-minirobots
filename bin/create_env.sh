#!/bin/bash

python -m venv env
if [[ $OS == *"Windows"* ]]; then
    source env/Scripts/activate
else
    source env/bin/activate
fi  
pip install --upgrade pip
pip install -r requirements.txt
deactivate