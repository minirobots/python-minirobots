#!/bin/bash

EDITING_MODE="${1:-emacs}"

if [ ! -d env ]; then
    bin/create_env.sh
fi

source env/bin/activate
ipython3 -i --TerminalInteractiveShell.editing_mode=${EDITING_MODE} minirobots.py
deactivate
