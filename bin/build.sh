#!/bin/bash

bin/clean.sh

echo "Running build..."
cp minirobots/turtle.py minirobots-tutorial/minirobots.py
python setup.py sdist bdist_wheel
