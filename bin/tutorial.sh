#!/bin/bash

if [ ! -d env ]; then
    bin/create_env.sh
fi


source env/bin/activate
ln -sf ../minirobots/turtle.py minirobots-tutorial/minirobots.py
JUPYTER_CONFIG_DIR=./minirobots-tutorial/.jupyter jupyter notebook ./minirobots-tutorial/Index.ipynb
rm -f minirobots-tutorial/minirobots.py
deactivate
