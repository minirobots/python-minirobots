#!/bin/bash

echo "Running clean..."
find . -type d -name __pycache__ -exec rm -rf '{}' \; 2>/dev/null
rm -rf __pycache__/ build/ dist env/ minirobots_turtle.egg-info/ .eggs
rm -f minirobots-tutorial/minirobots.py
